# C-Worthy

[![GoDoc](http://godoc.org/gitlab.com/CRThaze/cworthy?status.svg)](http://godoc.org/gitlab.com/CRThaze/cworthy)

Go implementations of some libc concepts with a few extra features.
