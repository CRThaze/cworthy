package exit

import (
	"gitlab.com/CRThaze/cworthy/errno"
)

// ExitT represents an unsigned 16 bit integer used to define exit statuses.
type ExitT errno.ErrorT

const (
	EX_OK          ExitT = 0  // successful termination
	EX__BASE       ExitT = 64 // base value for error messages
	EX_USAGE       ExitT = 64 // command line usage error
	EX_DATAERR     ExitT = 65 // data format error
	EX_NOINPUT     ExitT = 66 // cannot open input
	EX_NOUSER      ExitT = 67 // addressee unknown
	EX_NOHOST      ExitT = 68 // host name unknown
	EX_UNAVAILABLE ExitT = 69 // service unavailable
	EX_SOFTWARE    ExitT = 70 // internal software error
	EX_OSERR       ExitT = 71 // system error (e.g., can't fork)
	EX_OSFILE      ExitT = 72 // critical OS file missing
	EX_CANTCREAT   ExitT = 73 // can't create (user) output file
	EX_IOERR       ExitT = 74 // input/output error
	EX_TEMPFAIL    ExitT = 75 // temp failure; user is invited to retry
	EX_PROTOCOL    ExitT = 76 // remote error in protocol
	EX_NOPERM      ExitT = 77 // permission denied
	EX_CONFIG      ExitT = 78 // configuration error
	EX__MAX        ExitT = 78 // maximum listed value

	EX_ABORT ExitT = 134
)

var exitDescs = [^ExitT(0)]string{}

func init() {
	exitDescs[EX_OK] = "successful termination"
	exitDescs[EX_USAGE] = "command line usage error"
	exitDescs[EX_DATAERR] = "data format error"
	exitDescs[EX_NOINPUT] = "cannot open input"
	exitDescs[EX_NOUSER] = "addressee unknown"
	exitDescs[EX_NOHOST] = "host name unknown"
	exitDescs[EX_UNAVAILABLE] = "service unavailable"
	exitDescs[EX_SOFTWARE] = "internal software error"
	exitDescs[EX_OSERR] = "system error" // e.g. can't fork
	exitDescs[EX_OSFILE] = "critical OS file missing"
	exitDescs[EX_CANTCREAT] = "can't create (user) output file"
	exitDescs[EX_IOERR] = "input/output error"
	exitDescs[EX_TEMPFAIL] = "temp failure; user is invited to retry"
	exitDescs[EX_PROTOCOL] = "remote error in protocol"
	exitDescs[EX_NOPERM] = "permission denied"
	exitDescs[EX_CONFIG] = "configuration error"
	exitDescs[EX_ABORT] = "process aborted"
}

// Desc returns the description string for a given ExitCode.
func (e ExitT) Desc() string {
	return exitDescs[e]
}

// GetAllExitCodeDescriptions returns an array of all possible ExitCode description
// strings (where the index is the exit code). Any unreserved ExitCodes are also
// included with empty string descriptions.
func GetAllExitCodeDescriptions() [^ExitT(0)]string {
	return exitDescs
}
