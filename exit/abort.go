package exit

import (
	"os"
	"syscall"
)

func Abort() {
	proc, err := os.FindProcess(os.Getpid())
	if err != nil {
		panic(err)
	}
	proc.Signal(syscall.SIGABRT)
}
